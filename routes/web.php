<?php

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});
Route::get('/page1', function () {
    return view('page1');
})->name('page1');
Route::get('/page2', function () {
    return view('page2');
})->name('page2');
Route::get('/page3', function () {
    return view('page3');
})->name('page3');
Route::get('/page4', function () {
    return view('page4');
})->name('page4');
Route::get('/page5', function () {
    return view('page5');
})->name('page5');
Route::get('/page6', function () {
    return view('page6');
})->name('page6');
Route::get('/page7', function () {
    return view('page7');
})->name('page7');
Route::get('/page8', function () {
    return view('page8');
})->name('page8');
Route::get('/page9', function () {
    return view('page9');
})->name('page9');
Route::get('/page10', function () {
    return view('page10');
})->name('page10');
Route::get('/page11', function () {
    return view('page11');
})->name('page11');
Route::get('/page12', function () {
    return view('page12');
})->name('page12');
Route::get('/page13', function () {
    return view('page13');
})->name('page13');
Route::get('/page14', function () {
    return view('page14');
})->name('page14');
Route::get('/page15', function () {
    return view('page15');
})->name('page15');
Route::get('/page16', function () {
    return view('page16');
})->name('page16');
Route::get('/page17', function () {
    return view('page17');
})->name('page17');
Route::get('/page18', function () {
    return view('page18');
})->name('page18');
Route::get('/page19', function () {
    return view('page19');
})->name('page19');

Route::get('/page21', function () {
    return view('page21');
})->name('page21');
Route::get('/page22', function () {
    return view('page22');
})->name('page22');

Auth::routes();

Route::get('/home', [App\Http\Controllers\HomeController::class, 'index'])->name('home');
