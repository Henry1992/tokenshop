@extends('layouts.app')


@section('content')
    <div class="profilefinancewithdrawal">
        <div class="container">

            <div class="profile-menu">
                <ul class="menu-profile">
                    <li><a href="/page14">My NFT</a></li>
                    <li><a href="/page7">Subscribers</a></li>
                    <li><a href="/page8">Subscriptions</a></li>
                    <li><a class="action {{strpos(Route::current()->getName(),'page10')!==false?'selected':''}}"
                           href="{{route('page10')}}">My Finance</a></li>
                    <li><a href="/page11">Auctions/bids</a></li>
                    <li><a href="/page13">Notifications</a></li>
                    <li><a href="">Sign Out</a></li>
                </ul>
            </div>

{{--            <div class="profile-menu-mobile">--}}
{{--                <div id="pmm-button" >--}}
{{--                    <div style="display: flex;justify-content: space-between" id="button1" class="pmm-button">--}}
{{--                        <div class="button-text"> My Withdrawal </div>--}}
{{--                        <div class="polygon">▾</div>--}}
{{--                    </div>--}}
{{--                    <ul id="profile-menu-list" class="profile-menu-list" style="">--}}
{{--                        <li id="button2" style="padding-top: 9px; display: flex; justify-content: space-between; padding-right: 14px;">--}}
{{--                            <a class="action {{strpos(Route::current()->getName(),'page10')!==false?'selected':''}}"--}}
{{--                               href="{{route('page10')}}">My Withdrawal </a><div class="polygon-menu">▾</div></li>--}}
{{--                        <li><a href="">My NFT</a></li>--}}
{{--                        <li><a href="">Subscribers</a></li>--}}
{{--                        <li><a href="">My Finance</a></li>--}}
{{--                        <li><a href="">Auctions</a></li>--}}
{{--                        <li><a href="">Bids</a></li>--}}
{{--                        <li><a href="">Notifications</a></li>--}}
{{--                        <li><a href="">Sign Out</a></li>--}}
{{--                    </ul>--}}
{{--                </div>--}}
{{--            </div>--}}


            <div class="withdraw-box">

                <div class="arrow-box">
                    <a class="action {{strpos(Route::current()->getName(),'page9')!==false?'selected':''}}"
                       href="{{route('page9')}}" style="display: flex">
                    <div class="arrow"><img src="/images/right-arrow.png"/></div>
                    <div class="text"> Back </div>
                    </a>
                </div>

                <form>
                    <div class="title">To withdrawal please enter your data</div>
                    <div class="box">
                        <div class="input">
                            <input type="number" placeholder="Wallet number" readonly onfocus="this.removeAttribute('readonly')"/>
                        </div>

                        <div class="choose-menu">
                        <div id="choose" class="choose">
                            <button class="submit">Choose currency</button>
                            <div class="polygon">▾</div>
                        </div>
                        <ul id="choose-list" class="choose-list" style="">
                            <li id="choose2" class="choose2">
                                Choose currency<div class="polygon-menu">▾</div></li>
                            <li class="choose_check" data-value="Bitcoin">Bitcoin</li>
                            <div class="line-63B5E4"></div>
                            <li class="choose_check" data-value="Ethereum">Ethereum</li>
                            <div class="line-63B5E4"></div>
                            <li class="choose_check" data-value="Crypto.com Coin">Crypto.com Coin</li>
                        </ul>
                        </div>

                        <div class="input second">
                            <input type="number" placeholder="Enter Amount" readonly onfocus="this.removeAttribute('readonly')"/>
                        </div>
                    </div>

                    <div class="actions">
                        <button class="submit">Sign up</button>
                    </div>
                </form>

            </div>
        </div>
    </div>

@endsection
