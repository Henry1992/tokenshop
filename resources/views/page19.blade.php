@extends('layouts.app')


@section('content')

    <div class="explore">
        <div class="container">

            <div class="page-box">

                <div class="title">Discover NFTs</div>
                <div class="bar">
                    <div class="search-box">
                        <div class="bg-search">
                            <form class="searchbox">
                                <input type="search" class="search" placeholder="Search"/>
                                <button type="submit" class="submit" value="search">&nbsp;</button>
                            </form>
                        </div>
                    </div>
                    <div class="list">
                        <ul id="paragraph-sort" class="sortlist">
                            <li class="paragraph active">All items</li>
                            <li class="paragraph">Art</li>
                            <li class="paragraph">Gaming</li>
                            <li class="paragraph">Sport</li>
                            <li class="paragraph">Music</li>
                            <div id="paragraph-sort" class="paragraph-sort">
                                <div class="img"><img src="/images/sort.png"></div>
                                <div class="text">Sort by<span>&#9662;</span></div>
                            </div>
                        </ul>
                    </div>
                </div>
                <div class="slide">
                    <div class="left">
                        <div class="image"><img src="/images/explore1.png"></div>
                        <div class="title-price">
                            <div class="title-art">Name of Art</div>
                            <div class="price">120 USDT</div>
                        </div>
                        <div class="author-buy">
                            <div class="author">
                                <div class="author-avatar"><img src="/images/Rectangle26.png"></div>
                                <div class="nick-follow">
                                    <div class="usernickname">@Dbrw234</div>
                                    <div class="button-follow _show_modal" data-modal="#modalfollow">
                                        <div class="img-follower"><img src="/images/follower1.png"></div>
                                        <div class="text-follower">Follow</div>
                                    </div>
                                </div>
                            </div>
                            <div class="bid-buy">
                                <div class="endbuy">End in</div>
                                <div class="endtimer">3d 2h 45m</div>
                                <div class="buy">
                                    <button class="submit">Bid</button>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="center">
                        <div class="image"><img src="/images/explore2.png"></div>
                        <div class="title-price">
                            <div class="title-art">Name of Art</div>
                            <div class="price">120 USDT</div>
                        </div>
                        <div class="author-buy">
                            <div class="author">
                                <div class="author-avatar"><img src="/images/Rectangle26.png"></div>
                                <div class="nick-follow">
                                    <div class="usernickname">@Dbrw234</div>
                                    <div class="button-follow">
                                        <div class="img-following"><img src="/images/Vector5.png"></div>
                                        <div class="text-following">Following</div>
                                    </div>
                                </div>
                            </div>
                            <div class="bid-buy">
                                <div class="endbuy">&nbsp;</div>
                                <div class="endtimer">1 for Sale</div>
                                <div class="buy">
                                    <button class="submit">Buy</button>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="right">
                        <div class="image"><img src="/images/explore3.png"></div>
                        <div class="title-price">
                            <div class="title-art">Name of Art</div>
                            <div class="price">120 USDT</div>
                        </div>
                        <div class="author-buy">
                            <div class="author">
                                <div class="author-avatar"><img src="/images/Rectangle26.png"></div>
                                <div class="nick-follow">
                                    <div class="usernickname">@Dbrw234</div>
                                    <div class="button-follow _show_modal" data-modal="#modalfollow">
                                        <div class="img-follower"><img src="/images/follower1.png"></div>
                                        <div class="text-follower">Follow</div>
                                    </div>
                                </div>
                            </div>
                            <div class="bid-buy">
                                <div class="endbuy">End in</div>
                                <div class="endtimer">3d 2h 45m</div>
                                <div class="buy">
                                    <button class="submit">Bid</button>
                                </div>
                            </div>
                        </div>
                    </div>
                    {{--            <div class="slide-down">--}}
                    <div class="left">
                        <div class="image"><img src="/images/explore4.png"></div>
                        <div class="title-price">
                            <div class="title-art">Name of Art</div>
                            <div class="price">120 USDT</div>
                        </div>
                        <div class="author-buy">
                            <div class="author">
                                <div class="author-avatar"><img src="/images/Rectangle26.png"></div>
                                <div class="nick-follow">
                                    <div class="usernickname">@Dbrw234</div>
                                    <div class="button-follow _show_modal" data-modal="#modalfollow">
                                        <div class="img-follower"><img src="/images/follower1.png"></div>
                                        <div class="text-follower">Follow</div>
                                    </div>
                                </div>
                            </div>
                            <div class="bid-buy">
                                <div class="endbuy">End in</div>
                                <div class="endtimer">3d 2h 45m</div>
                                <div class="buy">
                                    <button class="submit">Bid</button>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="center">
                        <div class="image"><img src="/images/explore5.png"></div>
                        <div class="title-price">
                            <div class="title-art">Name of Art</div>
                            <div class="price">120 USDT</div>
                        </div>
                        <div class="author-buy">
                            <div class="author">
                                <div class="author-avatar"><img src="/images/Rectangle26.png"></div>
                                <div class="nick-follow">
                                    <div class="usernickname">@Dbrw234</div>
                                    <div class="button-follow1 _show_modal" data-modal="#modalfollow">
                                        <div class="img-follower"><img src="/images/follower1.png"></div>
                                        <div class="text-follower">Follow</div>
                                    </div>
                                </div>
                            </div>
                            <div class="bid-buy">
                                <div class="endbuy">&nbsp;</div>
                                <div class="endtimer">1 for Sale</div>
                                <div class="buy">
                                    <button class="submit">Buy</button>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="right">
                        <div class="image"><img src="/images/explore6.png"></div>
                        <div class="title-price">
                            <div class="title-art">Name of Art</div>
                            <div class="price">120 USDT</div>
                        </div>
                        <div class="author-buy">
                            <div class="author">
                                <div class="author-avatar"><img src="/images/Rectangle26.png"></div>
                                <div class="nick-follow">
                                    <div class="usernickname">@Dbrw234</div>
                                    <div class="button-follow _show_modal" data-modal="#modalfollow">
                                        <div class="img-follower"><img src="/images/follower1.png"></div>
                                        <div class="text-follower">Follow</div>
                                    </div>
                                </div>
                            </div>
                            <div class="bid-buy">
                                <div class="endbuy">End in</div>
                                <div class="endtimer">3d 2h 45m</div>
                                <div class="buy">
                                    <button class="submit">Bid</button>
                                </div>
                            </div>
                        </div>
                    </div>
                    {{--            </div>--}}
                </div>

                <div class="_modal" id="modalfollow">
                    <div class="_content">
                        <div class="modal_header" style="text-align: right;">
                            <div class="close _hide_modal">x</div>
                        </div>
                        <div class="body">
                            <div class="line_1">SUBSCRIBE AND GET THESE BENEFITS:</div>
                            <div class="line_2">Full access to the user’s content</div>
                            <div class="line_3">3.99 USDT/month</div>
                            <div class="line_4">Please enter your password to confirm subscription</div>
                            <div class="input_pass_modal">
                                <input placeholder="Confirm your password"/>
                            </div>
                            <div class="actions">
                                <button class="submit">CONFIRM SUBSCRIPTION</button>
                            </div>
                        </div>
                    </div>
                </div>
            </div>

        </div>
    </div>



@endsection
