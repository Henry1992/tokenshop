@extends('layouts.app')

@section('content')
    <div class="placeabid">
        <div class="container">

            <div class="content-container">

                <div class="content-box">

                    <div class="left">

                        <ul class="block-img">
                            <li class="select selected change_rotate" data-rotate="-90"><img src="/images/cube1.png"></li>
                            <li class="select change_rotate" data-rotate="90"><img src="/images/cube2.png"></li>
                            <li class="select change_rotate" data-rotate="-90"><img src="/images/cube3.png"></li>
                            <li class="select change_rotate" data-rotate="-180"><img src="/images/cube4.png"></li>
                            <li class="select change_rotate" data-rotate="-90"><img src="/images/cube5.png"></li>
                        </ul>
                        <div class="cube">

                            <div id="wrapD3Cube">
                                <div id="D3Cube">
                                    <div id="side1"
                                         style="background-image: url('/images/explore1.png')"></div>
                                    <div id="side2" style="background-image: url('/images/explore2.png')"></div>
                                    <div id="side3" style="background-image: url('/images/explore3.png')"></div>
                                    <div id="side4" style="background-image: url('/images/explore4.png')"></div>
                                    <div id="side5" style="background-image: url('/images/explore5.png')"></div>
                                    <div id="side6" style="background-image: url('/images/explore6.png')"></div>
                                </div>
                            </div>

                        </div>
                    </div>
                    <div class="right">

                        <div class="top-box">
                            <div class="author">
                                <div class="author-avatar"><img src="/images/Rectangle26.png"></div>
                                <div class="nick-follow">
                                    <div class="creator">Creator</div>
                                    <div class="usernickname">@Dbrw234</div>
                                </div>
                            </div>
                            <div class="prefix">
                                <div class="text">Art</div>
                                <div class="text">Fashion</div>
                            </div>
                        </div>

                        <div class="title-token">Name of Art</div>
                        <div class="prefix2">
                            <div class="text">Art</div>
                            <div class="text">Fashion</div>
                        </div>
                        <div class="line-63B5E4"></div>

                        <div class="timercash">
                            <div class="second">
                                <div class="current">CURRENT BID</div>
                                <div class="price">120 USDT</div>
                            </div>
                            <div class="second-right">
                                <div class="current">AUCTION ENDS IN</div>
                                <div class="price">3d 2h 45m</div>
                            </div>
                        </div>
                        <div class="line-63B5E4"></div>

                        <div class="description">
                            Description of Art Description of Art Description
                            of Art Description of Art Description of Art
                            Description of Art Description of Art Description of
                            Art Description of Art Description of Art
                            Description of Art Description of Art Description
                            of Art Description of Art Description of Art
                        </div>

                        <div class="quantity-box">
                            <div class="price-box-title">
                                <div class="quantity">Quantity</div>
                                <div class="flex-box">
                                    <div class="minus"> &ndash;</div>
                                    <div class="input">
                                        <input type="number" value="1" readonly class="hide_arrow"
                                               onfocus="this.removeAttribute('readonly')"/>
                                    </div>
                                    <div class="plus"> +</div>
                                </div>
                                {{--                                <div class="price-title">Bid</div>--}}
                            </div>
                            <div class="cols-box">
                                <div class="price-box-cash">
                                    {{--                                    <div class="flex-box">--}}
                                    {{--                                        <div class="minus"> &ndash;</div>--}}
                                    {{--                                        <div class="input">--}}
                                    {{--                                            <input type="number" value="1" readonly class="hide_arrow"--}}
                                    {{--                                                   onfocus="this.removeAttribute('readonly')"/>--}}
                                    {{--                                        </div>--}}
                                    {{--                                        <div class="plus"> +</div>--}}
                                    {{--                                    </div>--}}
                                    <div class="price-title">Bid</div>
                                    <div class="price-cash">
                                        <div style="display: flex">
                                            <div class="price-input">
                                                <input placeholder="Bid Amount" type="number" readonly
                                                       class="hide_arrow"
                                                       onfocus="this.removeAttribute('readonly')"/>
                                            </div>
                                            <div class="button-price">

                                                <div id="currencies-button3" class="button-currencies3"
                                                     style="position: relative;">
                                                    <div class="button-text"> Currencies ▾</div>
                                                    <ul id="button-currencies-list3" class="button-currencies-list3">
                                                        <li style="padding-top: 0">Currencies &#9206;</li>
                                                        <li class="color_b" data-value="BTC">BTC</li>
                                                        <div class="line-63B5E4"></div>
                                                        <li class="color_b" data-value="ETH">ETH</li>
                                                        <div class="line-63B5E4"></div>
                                                        <li class="color_b" data-value="CRO">CRO</li>
                                                    </ul>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="actions">
                                            <button class="submit">Place bid</button>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>

                        <div class="bids-title"> Bids</div>

                        <div class="bids-line">
                            <div class="avatar"><img src="/images/sechin.png"></div>
                            <div class="info">John Dow bid for</div>
                            <div class="cash">2 x 100BTC</div>
                            <div class="deal">Best Deal</div>
                        </div>
                        <div class="line-63B5E4"></div>

                        <div class="bids-line">
                            <div class="avatar"><img src="/images/sechin.png"></div>
                            <div class="info">John Dow bid for</div>
                            <div class="cash">1 x 40BTC</div>
                            <div class="deal"></div>
                        </div>
                        <div class="line-63B5E4"></div>

                        <div class="bids-line">
                            <div class="avatar"><img src="/images/sechin.png"></div>
                            <div class="info">John Dow bid for</div>
                            <div class="cash">3 x 10BTC</div>
                            <div class="deal"></div>
                        </div>
                        <div class="line-63B5E4"></div>

                    </div>

                </div>

            </div>


        </div>
    </div>
@endsection
