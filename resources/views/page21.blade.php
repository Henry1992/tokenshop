@extends('layouts.app')


@section('content')

    <div class="notifications">
        <div class="container">
            <div class="info-box">

                <div class="buttons-block">
                    <div class="mark-read">
                        <button class="submit">
                            <div class="img"><img src="/images/accept.png"></div>
                            <div class="text">Mark all as read</div>
                        </button>
                    </div>
                    <div class="delete-all">
                        <button class="submit">
                            <div class="img"><img src="/images/cancel.png"></div>
                            <div class="text">Delete All</div>
                        </button>
                    </div>
                </div>

                <div class="title-block">NEW</div>
                <div class="container-content">
                    <div class="information">
                        <div class="avatar"><img src="/images/sechin.png"></div>
                        <div class="info-text">John D. has been created new NFT. See ASAP</div>
                        <div class="buttons-ac">
                            <div class="accept"><img src="/images/accept.png"></div>
                            <div class="cancel"><img src="/images/cancel.png"></div>
                        </div>
                    </div>
                    <div class="blue-line"></div>
                    <div class="information">
                        <div class="avatar"><img src="/images/sechin.png"></div>
                        <div class="info-text">John D. made bid to your NFT. Let’s see ASAP</div>
                        <div class="buttons-ac">
                            <div class="accept"><img src="/images/accept.png"></div>
                            <div class="cancel"><img src="/images/cancel.png"></div>
                        </div>
                    </div>
                    <div class="blue-line"></div>
                    <div class="information">
                        <div class="avatar"><img src="/images/sechin.png"></div>
                        <div class="info-text">John D. is your new subscriber</div>
                        <div class="buttons-ac">
                            <div class="accept"><img src="/images/accept.png"></div>
                            <div class="cancel"><img src="/images/cancel.png"></div>
                        </div>
                    </div>
                    <div class="blue-line"></div>
                    <div class="information">
                        <div class="avatar"><img src="/images/sechin.png"></div>
                        <div class="info-text">John D. brought your NFT. Let’s create new one</div>
                        <div class="buttons-ac">
                            <div class="accept"><img src="/images/accept.png"></div>
                            <div class="cancel"><img src="/images/cancel.png"></div>
                        </div>
                    </div>
                    <div class="blue-line"></div>
                    <div class="information">
                        <div class="avatar"><img src="/images/sechin.png"></div>
                        <div class="info-text">John D. your bid won.Lets look your NFT</div>
                        <div class="buttons-ac">
                            <div class="accept"><img src="/images/accept.png"></div>
                            <div class="cancel"><img src="/images/cancel.png"></div>
                        </div>
                    </div>
                </div>
                <div class="title-block">EARLIER</div>
                <div class="container-content">
                    <div class="information">
                        <div class="avatar"><img src="/images/sechin.png"></div>
                        <div class="info-text">John D. has been created new NFT. See ASAP</div>
                        <div class="buttons-ac">
                            <div class="cancel"><img src="/images/cancel.png"></div>
                        </div>
                    </div>
                    <div class="blue-line"></div>
                    <div class="information">
                        <div class="avatar"><img src="/images/sechin.png"></div>
                        <div class="info-text">John D. made bid to your NFT. Let’s see ASAP</div>
                        <div class="buttons-ac">
                            <div class="cancel"><img src="/images/cancel.png"></div>
                        </div>
                    </div>
                    <div class="blue-line"></div>
                    <div class="information">
                        <div class="avatar"><img src="/images/sechin.png"></div>
                        <div class="info-text">John D. is your new subscriber</div>
                        <div class="buttons-ac">
                            <div class="cancel"><img src="/images/cancel.png"></div>
                        </div>
                    </div>
                </div>

            </div>
        </div>
    </div>

@endsection
