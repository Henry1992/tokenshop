@extends('layouts.app')


@section('content')
    <div class="profilebids">
        <div class="container">

            <div class="profile-menu">
                <ul class="menu-profile">
                    <li><a href="/page14">My NFT</a></li>
                    <li><a href="/page7">Subscribers</a></li>
                    <li><a href="/page8">Subscriptions</a></li>
                    <li><a href="/page9">My Finance</a></li>
                    <li><a class="action {{strpos(Route::current()->getName(),'page11')!==false?'selected':''}}"
                           href="{{route('page11')}}">Auctions/bids</a></li>
                    <li><a href="/page13">Notifications</a></li>
                    <li><a href="">Sign Out</a></li>
                </ul>
            </div>
            <div class="profile-menu-mobile">
                <div id="pmm-button" >
                    <div style="display: flex;justify-content: space-between" id="button1" class="pmm-button">
                        <div class="button-text"> Bids </div>
                        <div class="polygon">▾</div>
                    </div>
                    <ul id="profile-menu-list" class="profile-menu-list" style="">
                        <li id="button2" style="padding-top: 9px; display: flex; justify-content: space-between; padding-right: 14px;">
                            <a class="action {{strpos(Route::current()->getName(),'page12')!==false?'selected':''}}"
                               href="{{route('page12')}}">Bids </a><div class="polygon-menu">▾</div></li>
                        <li><a href="/page6">My Account</a></li>
                        <li><a href="/page14">My Created NFT</a></li>
                        <li><a href="/page15">My Owned NFT</a></li>
                        <li><a href="/page7">Subscribers</a></li>
                        <li><a href="/page8">Subscriptions</a></li>
                        <li><a href="/page9">My Finance</a></li>
                        <li><a href="/page11">Auctions</a></li>
                        <li><a href="/page13">Notifications</a></li>
                        <li><a href="">Sign Out</a></li>
                    </ul>
                </div>
            </div>

            <div class="profile-box">
                <div class="photo">
                    <img src="/images/pexels.png">
                    <div class="elipse"><img src="/images/Ellipse.png"></div>
                    <div class="camera"><img src="/images/camera.png"></div>
                </div>
                <div class="info-box">
                    <div class="top-box">
                        <div class="username">John Dow</div>
                        <div class="actions">
                            <button class="submit">
                                <div class="pencil"><img src="/images/pencil.png"></div>
                                <div class="edit-text"> Edit Profile </div>
                            </button>
                        </div>
                    </div>
                    <div class="photo-mobile">
                        <img src="/images/pexels.png">
                    </div>
                    <div class="username-mobile">John Dow</div>
                    <div class="counters">
                        <div class="count-box">
                            <div class="count">12</div>
                            <div class="text">Creations</div>
                        </div>
                        <div class="count-box">
                            <div class="count">234</div>
                            <div class="text">Subscribers</div>
                        </div>
                        <div class="count-box">
                            <div class="count">123</div>
                            <div class="text">Subscribed</div>
                        </div>
                    </div>
                    <div class="descriptions">

                        <p>Description of Profile Description of Profile Description of Profile Description of Profile Description of Profile Description of Profile</p>
                        <p>Description of Profile Description of Profile Description of Profile Description of Profile Description of Profile Description of Profile</p>

                    </div>
                </div>

                <div class="subscribers-block">
                    <div class="buttons-switch">
                        <div class="actions-2">
                            <button class="submit">
{{--                                <div class="edit-text"> Auctions </div>--}}
                                <a class="action {{strpos(Route::current()->getName(),'page11')!==false?'selected':''}}"
                                   href="{{route('page11')}}">Auctions </a>
                            </button>
                        </div>
                        <div class="actions">
                            <button class="submit">
                                <div class="edit-text"> Bids </div>
                            </button>
                        </div>
                    </div>

                    <div class="content" style="margin-top: 19px;">
                        <div class="photolot"><img src="/images/explore5.png"> </div>
                        <div class="text-info">
                            <div class="text">Jonhn D.<br /> make bid 2133 USDT * 3 NFT</div>
                            <div class="text">Jonhn D.<br /> make bid 2133 USDT * 4 NFT</div>
                            <div class="text">Jonhn D.<br /> make bid 933 USDT * 5 NFT</div>
                            <div class="text">Jonhn D.<br /> make bid 1133 USDT * 3 NFT</div>
                            <div class="text">Jonhn D.<br /> make bid 2133 USDT * 3 NFT</div>
                        </div>
                    </div>
                    <div class="line-63B5E4"></div>

                    <div class="content">
                        <div class="photolot"><img src="/images/explore5.png"> </div>
                        <div class="text-info">
                            <div class="text">Jonhn D. make bid 2133 USDT * 3 NFT</div>
                            <div class="text">Jonhn D. make bid 2133 USDT * 4 NFT</div>
                            <div class="text">Jonhn D. make bid 933 USDT * 5 NFT</div>
                            <div class="text">Jonhn D. make bid 1133 USDT * 3 NFT</div>
                            <div class="text">Jonhn D. make bid 2133 USDT * 3 NFT</div>
                        </div>
                    </div>
                    <div class="line-63B5E4"></div>

                    <div class="content">
                        <div class="photolot"><img src="/images/explore5.png"> </div>
                        <div class="text-info">
                            <div class="text">Jonhn D. make bid 2133 USDT * 3 NFT</div>
                            <div class="text">Jonhn D. make bid 2133 USDT * 4 NFT</div>
                            <div class="text">Jonhn D. make bid 933 USDT * 5 NFT</div>
                            <div class="text">Jonhn D. make bid 1133 USDT * 3 NFT</div>
                            <div class="text">Jonhn D. make bid 2133 USDT * 3 NFT</div>
                        </div>
                    </div>
                    <div class="line-63B5E4"></div>

                </div>
            </div>

        </div>
    </div>

@endsection
