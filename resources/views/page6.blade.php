@extends('layouts.app')


@section('content')
    <div class="profileedit">
        <div class="container">

            <div class="profile-menu">
                <ul class="menu-profile">
                    <li><a href="/page14">My NFT</a></li>
                    <li><a href="/page7">Subscribers</a></li>
                    <li><a href="/page8">Subscriptions</a></li>
                    <li><a href="/page9">My Finance</a></li>
                    <li><a href="/page11">Auctions/bids</a></li>
                    <li><a href="/page13">Notifications</a></li>
                    <li><a href="">Sign Out</a></li>
                </ul>
            </div>

            <div class="profile-menu-mobile">
                <div id="pmm-button" >
                    <div style="display: flex;justify-content: space-between" id="button1" class="pmm-button">
                        <div class="button-text"> My Account </div>
                        <div class="polygon">▾</div>
                    </div>
                    <ul id="profile-menu-list" class="profile-menu-list" style="">
                        <li id="button2" style="padding-top: 9px; display: flex; justify-content: space-between; padding-right: 14px;">
                            <a class="action {{strpos(Route::current()->getName(),'page6')!==false?'selected':''}}"
                               href="{{route('page6')}}">My Account </a><div class="polygon-menu">▾</div></li>
                        <li><a href="/page14">My Created NFT</a></li>
                        <li><a href="/page15">My Owned NFT</a></li>
                        <li><a href="/page7">Subscribers</a></li>
                        <li><a href="/page8">Subscriptions</a></li>
                        <li><a href="/page9">My Finance</a></li>
                        <li><a href="/page11">Auctions</a></li>
                        <li><a href="/page12">Bids</a></li>
                        <li><a href="/page13">Notifications</a></li>
                        <li><a href="">Sign Out</a></li>
                    </ul>
                </div>
            </div>

            <div class="profile-box">

                <div class="photo">
                    <img src="/images/pexels.png">
                    <div class="elipse"><img src="/images/Ellipse.png"></div>
                    <div class="camera"><img src="/images/camera.png"></div>
                </div>

                <div class="info-box">
                    <div class="top-box">
                        <div class="username">John Dow</div>
                        <div class="actions">
                            <button class="submit">
                                <div class="pencil"><img src="/images/pencil.png"></div>
                                <div class="edit-text"> Edit Profile </div>
                            </button>
                        </div>
                    </div>

                    <div class="photo-mobile">
                        <img src="/images/pexels.png">
                    </div>

                    <div class="username-mobile">John Dow</div>

                    <div class="counters">
                        <div class="count-box">
                            <div class="count">12</div>
                            <div class="text">Creations</div>
                        </div>
                        <div class="count-box">
                            <div class="count">234</div>
                            <div class="text">Subscribers</div>
                        </div>
                        <div class="count-box">
                            <div class="count">123</div>
                            <div class="text">Subscribed</div>
                        </div>
                    </div>

                    <div class="descriptions">

                        <p>Description of Profile Description of Profile Description of Profile Description of Profile Description of Profile Description of Profile</p>
                        <p>Description of Profile Description of Profile Description of Profile Description of Profile Description of Profile Description of Profile</p>

                    </div>
                </div>
            </div>

        </div>
    </div>

    <div class="popinfo">
        <div class="blockinfo">
            <div class="box">
                <div class="text"> John D. is your new subscriber </div>
                <div class="close"> x </div>
            </div>
        </div>
        <div class="blockinfo">
            <div class="box">
                <div class="text"> John D. is your new subscriber </div>
                <div class="close"> x </div>
            </div>
        </div>
    </div>

@endsection
