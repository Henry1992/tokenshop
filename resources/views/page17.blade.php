@extends('layouts.app')

@section('content')
    <div class="buyitnow">
        <div class="container">

            <div class="content-container">

                <div class="content-box">

                    <div class="left">

                        <ul class="block-img">
                            <li class="select selected change_rotate" data-rotate="-90"><img src="/images/cube1.png"></li>
                            <li class="select change_rotate" data-rotate="90"><img src="/images/cube2.png"></li>
                            <li class="select change_rotate" data-rotate="-90"><img src="/images/cube3.png"></li>
                            <li class="select change_rotate" data-rotate="-180"><img src="/images/cube4.png"></li>
                            <li class="select change_rotate" data-rotate="-90"><img src="/images/cube5.png"></li>
                        </ul>
                        <div class="cube">

                            <div id="wrapD3Cube">
                                <div id="D3Cube">
                                    <div id="side1"
                                         style="background-image: url('/images/explore1.png')"></div>
                                    <div id="side2" style="background-image: url('/images/explore2.png')"></div>
                                    <div id="side3" style="background-image: url('/images/explore3.png')"></div>
                                    <div id="side4" style="background-image: url('/images/explore4.png')"></div>
                                    <div id="side5" style="background-image: url('/images/explore5.png')"></div>
                                    <div id="side6" style="background-image: url('/images/explore6.png')"></div>
                                </div>
                            </div>

                        </div>
                    </div>
                    <div class="right">

                        <div class="top-box">
                            <div class="author">
                                <div class="author-avatar"><img src="/images/Rectangle26.png"></div>
                                <div class="nick-follow">
                                    <div class="creator">Creator</div>
                                    <div class="usernickname">@Dbrw234</div>
                                </div>
                            </div>
                            <div class="prefix">
                                <div class="text">Art</div>
                                <div class="text">Fashion</div>
                            </div>
                        </div>

                        <div class="title-token">Name of Art</div>
                        <div class="prefix2">
                            <div class="text">Art</div>
                            <div class="text">Fashion</div>
                        </div>
                        <div class="line-63B5E4"></div>

                        <div class="second">
                            <div class="sale">6 for Sale</div>
                            <div class="price">120 USDT</div>
                        </div>
                        <div class="line-63B5E4"></div>

                        <div class="description">
                            Description of Art Description of Art Description
                            of Art Description of Art Description of Art
                            Description of Art Description of Art Description of
                            Art Description of Art Description of Art
                            Description of Art  Description of Art Description
                            of Art Description of Art Description of Art
                        </div>

                        <div class="quantity-box">
                            <div class="price-box-title">
                                <div class="quantity">Quantity</div>
                                <div class="price-title">Price</div>
                            </div>
                            <div class="cols-box">
                                <div class="price-box-cash">
                                    <div class="flex-box">
                                        <div class="minus"> &ndash; </div>
                                        <div class="input">
                                            <input type="number" value="1" readonly class="hide_arrow"
                                                   onfocus="this.removeAttribute('readonly')"/>
                                        </div>
                                        <div class="plus"> + </div>
                                    </div>
                                    <div class="price-cash">
                                        <div class="cash-cash" data-price="250">120</div>
                                        <div class="cash-title">USDT</div>
                                    </div>
                                </div>
                            </div>
                        </div>

                        <div class="actions">
                            <button class="submit">Buy it now</button>
                        </div>

                    </div>

                </div>

            </div>



        </div>
    </div>
@endsection
