@extends('layouts.app')


@section('content')

    <div class="home">
        <div class="container">

            <div class="activesort">
                <ul id="activesort" class="sortlist">
                    <li class="nft_sort active">Today</li>
                    <li class="nft_sort">Last 7 days</li>
                    <li class="nft_sort">Last 30 days</li>
                    <li class="nft_sort" style="margin-right: 0;">All Time</li>
                </ul>
            </div>

            <div class="container-content">
                <div class="cube"><img src="/images/cubexxl.png"> </div>
                <div class="information">
                    <div class="author">
                        <div class="author-avatar"><img src="/images/Rectangle26.png"></div>
                        <div class="nick-follow">
                            <div class="creator">Creator</div>
                            <div class="usernickname">@Dbrw234</div>
                        </div>
                    </div>
                    <div class="title-token">Name of Art</div>
                    <div class="prefix">
                        <div class="text">Art</div>
                        <div class="text">Fashion</div>
                    </div>
                    <div class="endtimer">
                        <div class="text">End in</div>
                        <div class="timer">3d 2h 45m</div>
                    </div>
                    <div class="infobid">Minimum Bid 89 USDT </div>
                    <div class="actions">
                        <button class="submit">PLACE A BID</button>
                    </div>
                </div>
            </div>
            <div class="container-content">
                <div class="cube"><img src="/images/cubexxl.png"> </div>
                <div class="information">
                    <div class="author">
                        <div class="author-avatar"><img src="/images/Rectangle26.png"></div>
                        <div class="nick-follow">
                            <div class="creator">Creator</div>
                            <div class="usernickname">@Dbrw234</div>
                        </div>
                    </div>
                    <div class="title-token">Name of Art</div>
                    <div class="prefix">
                        <div class="text">Art</div>
                        <div class="text">Fashion</div>
                    </div>
                    <div class="endtimer" style="display: block">
{{--                        <div class="text">End in</div>--}}
{{--                        <div class="timer">3d 2h 45m</div>--}}
                        <div class="second">
                        <div class="sale">6 for Sale</div>
                        <div class="price">120 USDT</div>
                        </div>
                    </div>
                    <div class="actions">
                        <button class="submit">PLACE A BID</button>
                    </div>
                </div>
            </div>
            <div class="container-content">
                <div class="cube"><img src="/images/cubexxl.png"> </div>
                <div class="information">
                    <div class="author">
                        <div class="author-avatar"><img src="/images/Rectangle26.png"></div>
                        <div class="nick-follow">
                            <div class="creator">Creator</div>
                            <div class="usernickname">@Dbrw234</div>
                        </div>
                    </div>
                    <div class="title-token">Name of Art</div>
                    <div class="prefix">
                        <div class="text">Art</div>
                        <div class="text">Fashion</div>
                    </div>
                    <div class="endtimer" style="display: block">
                        {{--                        <div class="text">End in</div>--}}
                        {{--                        <div class="timer">3d 2h 45m</div>--}}
                        <div class="second">
                            <div class="sale">6 for Sale</div>
                            <div class="price">120 USDT</div>
                        </div>
                    </div>
                    <div class="actions">
                        <button class="submit">PLACE A BID</button>
                    </div>
                </div>
            </div>

        </div>
    </div>

@endsection
