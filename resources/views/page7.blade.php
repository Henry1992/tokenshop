@extends('layouts.app')


@section('content')
    <div class="profilesubscribers">
        <div class="container">

            <div class="profile-menu">
                <ul class="menu-profile">
                    <li><a href="/page14">My NFT</a></li>
                    <li><a class="action {{strpos(Route::current()->getName(),'page7')!==false?'selected':''}}"
                           href="{{route('page7')}}">Subscribers</a></li>
                    <li><a href="/page8">Subscriptions</a></li>
                    <li><a href="/page9">My Finance</a></li>
                    <li><a href="/page11">Auctions/bids</a></li>
                    <li><a href="/page13">Notifications</a></li>
                    <li><a href="">Sign Out</a></li>
                </ul>
            </div>
            <div class="profile-menu-mobile">
                <div id="pmm-button" >
                    <div style="display: flex;justify-content: space-between" id="button1" class="pmm-button">
                        <div class="button-text"> Subscribers </div>
                        <div class="polygon">▾</div>
                    </div>
                    <ul id="profile-menu-list" class="profile-menu-list" style="">
                        <li id="button2" style="padding-top: 9px; display: flex; justify-content: space-between; padding-right: 14px;">
                            <a class="action {{strpos(Route::current()->getName(),'page7')!==false?'selected':''}}"
                               href="{{route('page7')}}">Subscribers </a><div class="polygon-menu">▾</div></li>
                        <li><a href="/page6">My Account</a></li>
                        <li><a href="/page14">My Created NFT</a></li>
                        <li><a href="/page15">My Owned NFT</a></li>
                        <li><a href="/page8">Subscriptions</a></li>
                        <li><a href="/page9">My Finance</a></li>
                        <li><a href="/page11">Auctions</a></li>
                        <li><a href="/page12">Bids</a></li>
                        <li><a href="/page13">Notifications</a></li>
                        <li><a href="">Sign Out</a></li>
                    </ul>
                </div>
            </div>

            <div class="profile-box">
                <div class="photo">
                    <img src="/images/pexels.png">
                    <div class="elipse"><img src="/images/Ellipse.png"></div>
                    <div class="camera"><img src="/images/camera.png"></div>
                </div>
                <div class="info-box">
                    <div class="top-box">
                        <div class="username">John Dow</div>
                        <div class="actions">
                            <button class="submit">
                                <div class="pencil"><img src="/images/pencil.png"></div>
                                <div class="edit-text"> Edit Profile</div>
                            </button>
                        </div>
                    </div>
                    <div class="photo-mobile">
                        <img src="/images/pexels.png">
                    </div>
                    <div class="username-mobile">John Dow</div>
                    <div class="counters">
                        <div class="count-box">
                            <div class="count">12</div>
                            <div class="text">Creations</div>
                        </div>
                        <div class="count-box">
                            <div class="count">234</div>
                            <div class="text">Subscribers</div>
                        </div>
                        <div class="count-box">
                            <div class="count">123</div>
                            <div class="text">Subscribed</div>
                        </div>
                    </div>
                    <div class="descriptions">

                        <p>Description of Profile Description of Profile Description of Profile Description of Profile
                            Description of Profile Description of Profile</p>
                        <p>Description of Profile Description of Profile Description of Profile Description of Profile
                            Description of Profile Description of Profile</p>

                    </div>
                </div>

                <div class="subscribers-block">
                    <div class="line-63B5E4"></div>

                    <div class="subscription-box">
                        <div class="title-text" style="margin-right: 22px;">SUBSCRIPTION PRICE</div>
                        {{--                        <div class="button-price">3.99 USDT/MONTH ▾</div>--}}
                        <div class="button-price">

                            <div id="bp-button" class="button-price" style="position: relative;">
                                <div class="button-text"><span id="price_value">3.99</span> USDT/MONTH ▾</div>
                                <ul id="button-price-list" class="button-price-list">
                                    <li class="price_check" data-value="3.99">3.99 USDT/MONTH &#9206;</li>
                                    <li class="price_check" data-value="5.99">5.99 USDT/MONTH</li>
                                    <li class="price_check" data-value="13.99">13.99 USDT/MONTH</li>
                                </ul>
                            </div>
                        </div>
                    </div>

                    <div class="hr">or enter your price</div>

                    <div class="lineenteryour">
                        <div class="line1"></div>
                        <div class="entertext">or enter your price</div>
                        <div class="line2"></div>
                    </div>

                    <div class="newprice-box">
                        <div class="title-text1" style="margin-right: 50px;">ENTER NEW PRICE</div>
                     <div class="mobipriceinp">
                         <div class="price-input">
                             <input placeholder="Your Price" type="number" readonly class="hide_arrow"
                                    onfocus="this.removeAttribute('readonly')"/>
                         </div>
                         <div class="title-text">USDT/MONTH</div>
                     </div>
                    </div>

                    <div class="actions">
                        <button class="submit">Submit</button>
                    </div>

                    <div class="line-63B5E4-2"></div>

                    <div class="subs-title">SUBSCRIBERS</div>

                    <div class="subs-list">

                        <div class="subs-box">
                            <span> <img src="/images/sechin.png"> </span>
                            <p> John S. </p>
                        </div>
                        <div class="subs-box">
                            <span> <img src="/images/marina.png"> </span>
                            <p> John S. </p>
                        </div>
                        <div class="subs-box">
                            <span> <img src="/images/kita.png"> </span>
                            <p> John S. </p>
                        </div>
                        <div class="subs-box">
                            <span> <img src="/images/ponteleymon.png"> </span>
                            <p> John S. </p>
                        </div>
                        <div class="subs-box">
                            <span> <img src="/images/sechin.png"> </span>
                            <p> John S. </p>
                        </div>
                        <div class="subs-box">
                            <span> <img src="/images/ivanich.png"> </span>
                            <p> John S. </p>
                        </div>
                        <div class="subs-box">
                            <span> <img src="/images/babanin.png"> </span>
                            <p> John S. </p>
                        </div>
                        <div class="subs-box">
                            <span> <img src="/images/marina.png"> </span>
                            <p> John S. </p>
                        </div>
                        <div class="subs-box">
                            <span> <img src="/images/kita.png"> </span>
                            <p> John S. </p>
                        </div>
                        <div class="subs-box">
                            <span> <img src="/images/kudrya.png"> </span>
                            <p> John S. </p>
                        </div>
                        <div class="subs-box">
                            <span> <img src="/images/ponteleymon.png"> </span>
                            <p> John S. </p>
                        </div>
                        <div class="subs-box">
                            <span> <img src="/images/ivanich.png"> </span>
                            <p> John S. </p>
                        </div>


                    </div>
                </div>
            </div>

        </div>
    </div>

@endsection

