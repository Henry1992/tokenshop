@extends('layouts.app')


@section('content')
    <div class="addnft1">
        <div class="container">

            <form class="nft-cont">

                <div class="arrow-x">
                    <div class="arrow-box" data-step="prev">
                        <div class="arrow"><img src="/images/right-arrow.png"/></div>
                        <div class="text"> Back</div>
                    </div>
                    <div id="close-x" class="close-x _show_modal" data-modal="#modaladdnftclose">x</div>
                </div>

                <div class="title">Create your NFT in 5 simple steps</div>

                <div class="step_container step1" data-step="1">

                    <div class="title-mob">Create your NFT in 5 simple steps</div>

                    <div class="flex-info">
                        <div class="video-box">
                            <div class="video">
                                <div class="move">
                                    <div style="margin: auto;">
                                        <img style="width: 100px; height: 100px;" src="/images/move.png"/>
                                    </div>
                                </div>
                                <img src="/images/image21.png"/>
                            </div>
                        </div>
                        <div class="info-box">

                            <div class="step">Step 1</div>
                            <div class="description">Drop here a short 10-15 seconds video with your NFT</div>
                            <div class="requrements">(requrements)</div>
                            <div class="actions">
                                <button class="submit">Upload</button>
                            </div>

                            <div class="video-box-mobile">
                                <div class="video">
                                    <div class="move">
                                        <div style="margin: auto;">
                                            <img style="width: 100px; height: 100px;" src="/images/move.png"/>
                                        </div>
                                    </div>
                                    <img src="/images/image21.png"/>
                                </div>
                            </div>
                        </div>
                    </div>

                </div>

                <div class="step_container step2" data-step="2">

                    <div class="step2-content">

                        <div class="left">
                            <div class="video-box">
                                <div class="video">
                                    <div class="move">
                                        <div style="margin: auto; margin-top: 38%;">
                                            <img style="width: 159px; height: 130px;" src="/images/upl.png"/>
                                        </div>
                                        <div class="actions">
                                            <button class="submit">Upload</button>
                                        </div>
                                    </div>
                                    <img src="/images/whiteupl.png"/>
                                </div>
                            </div>

                        </div>

                        <div class="right">

                            <div class="margin">
                                <div class="title-step">STEP 2</div>
                                <div class="description">Drop cover image for your future NFT, add description and
                                    hashtags
                                </div>
                            </div>
                            <div class="actions">
                                <button class="submit">Upload</button>
                            </div>
                            <div class="margin">
                                <div class="requrements">NFT despcription</div>
                                <div class="input">
                                    <textarea name="message" cols="30" rows="6" placeholder="Text input"></textarea>
                                </div>
                                <div class="requrements2">NFT hashtags</div>
                                <div class="input">
                                    <textarea name="message" cols="30" rows="3" placeholder="#art #gaming"></textarea>
                                </div>
                            </div>

                        </div>

                    </div>

                </div>

                <div class="step_container step3" data-step="3">

                    <div class="block">
                        {{--                        <div class="left"></div>--}}
                        <div class="center">
                            <div class="title-step">STEP 3</div>
                            <div class="description">Add your social media here</div>
                            <div class="box">
                                <div class="soc-line">
                                    <img src="/images/fbi.png">
                                    <div class="input_soc">
                                        <input placeholder="Text input" readonly
                                               onfocus="this.removeAttribute('readonly')"/>
                                    </div>
                                </div>
                                <div class="soc-line">
                                    <img src="/images/insti.png">
                                    <div class="input_soc">
                                        <input placeholder="Text input" readonly
                                               onfocus="this.removeAttribute('readonly')"/>
                                    </div>
                                </div>
                                <div class="soc-line">
                                    <img src="/images/twiti.png">
                                    <div class="input_soc">
                                        <input placeholder="Text input" readonly
                                               onfocus="this.removeAttribute('readonly')"/>
                                    </div>
                                </div>
                                <div class="soc-line">
                                    <img src="/images/ini.png">
                                    <div class="input_soc">
                                        <input placeholder="Text input" readonly
                                               onfocus="this.removeAttribute('readonly')"/>
                                    </div>
                                </div>
                                <div class="soc-line">
                                    <img src="/images/pini.png">
                                    <div class="input_soc">
                                        <input placeholder="Text input" readonly
                                               onfocus="this.removeAttribute('readonly')"/>
                                    </div>
                                </div>
                                <div class="soc-line second_inp">
                                    <img src="/images/bei.png">
                                    <div class="input_soc">
                                        <input placeholder="Text input" readonly
                                               onfocus="this.removeAttribute('readonly')"/>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>

                </div>

                <div class="step_container step4" data-step="4">

                    <div class="block">
                        <div class="box">
                            <div class="title-step">STEP 4</div>

                            <div class="description">
                                How many copies do you want<br/> to create?
                                <div class="cols-box">
                                    <div class="price-box-cash">
                                        <div class="flex-box">
                                            <div class="minus"> &ndash;</div>
                                            <div class="input">
                                                <input type="number" value="1" readonly class="hide_arrow"
                                                       onfocus="this.removeAttribute('readonly')"/>
                                            </div>
                                            <div class="plus"> +</div>
                                        </div>
                                    </div>
                                </div>
                            </div>

                            <div class="first-box">

                                <div class="information"></div>
                                <div class="price-input">
                                    <input placeholder="Your Price" type="number" readonly class="hide_arrow"
                                           onfocus="this.removeAttribute('readonly')"/>
                                </div>
                                <div class="button-price">

                                    <div id="currencies-button" class="button-currencies" style="position: relative;">
                                        <div class="button-text"> Currencies ▾</div>
                                        <ul id="button-currencies-list" class="button-currencies-list">
                                            <li style="padding-top: 0">Currencies &#9206;</li>
                                            <li class="color_b3" data-value="BTC">BTC</li>
                                            <div class="line-63B5E4"></div>
                                            <li class="color_b3" data-value="ETH">ETH</li>
                                            <div class="line-63B5E4"></div>
                                            <li class="color_b3" data-value="CRO">CRO</li>
                                        </ul>
                                    </div>
                                </div>

                            </div>

                            <div class="lineenteryour">
                                <div class="line1"></div>
                                <div class="entertext" style="margin-right: 13px;">or bid price</div>
                                <div class="line2"></div>
                            </div>

                            <div class="second-box">

                                <div class="information" style="margin: unset"></div>
                                <div class="block-box">
                                    <div class="title-bmp">Bid</div>
                                    <div class="title-bmp">Minimum price</div>
                                    <div class="flex-box-curr">
                                        <div class="price-input">
                                            <input placeholder="Your Price" type="number" readonly class="hide_arrow"
                                                   onfocus="this.removeAttribute('readonly')"/>
                                        </div>
                                        <div class="button-price">

                                            <div id="currencies-button2" class="button-currencies2"
                                                 style="position: relative;">
                                                <div class="button-text"> Currencies ▾</div>
                                                <ul id="button-currencies-list2" class="button-currencies-list2">
                                                    <li style="padding-top: 0">Currencies &#9206;</li>
                                                    <li class="color_b2" data-value="BTC">BTC</li>
                                                    <div class="line-63B5E4"></div>
                                                    <li class="color_b2" data-value="ETH">ETH</li>
                                                    <div class="line-63B5E4"></div>
                                                    <li class="color_b2" data-value="CRO">CRO</li>
                                                </ul>
                                            </div>
                                        </div>
                                    </div>
                                </div>

                            </div>

                            <div class="lineenteryour">
                                <div class="line1"></div>
                                <div class="entertext">Royalty</div>
                                <div id="quest-button" class="questmark">?
                                    <div id="quest-text" class="quest-text">“Royalties” are rewards to the creator, that
                                        are recieved each time when their art is sold and bought again
                                    </div>
                                </div>
                                <div class="line2"></div>
                            </div>

                            <div class="bottom-box">
                                <div class="procent-box">

                                    <div class="procent"><div class="information-active"></div>10%</div>
                                </div>
                                <div class="procent-box">
                                    <div class="procent"><div class="information-active"></div>20%</div>
                                </div>
                                <div class="procent-box">
                                    <div class="procent"><div class="information-active"></div>30%</div>
                                </div>
                            </div>

                            <div class="bottom-text">
                                *20% royalties is recommended for works of art.
                            </div>

                        </div>
                    </div>

                </div>

                <div class="step_container step5" data-step="5">
                    <div class="content-container">

                        <div class="content-box">

                            <div class="title-step-m">STEP 5</div>

                            <div class="left">

                                <ul class="block-img">
                                    <li class="select selected change_rotate" data-rotate="-90"><img src="/images/explore1.png"></li>
                                    <li class="select change_rotate" data-rotate="90"><img src="/images/explore2.png"></li>
                                    <li class="select change_rotate" data-rotate="-90"><img src="/images/explore3.png"></li>
                                    <li class="select change_rotate" data-rotate="-180"><img src="/images/explore4.png"></li>
                                    <li class="select change_rotate" data-rotate="-90"><img src="/images/explore5.png"></li>
                                </ul>
                                <div class="cube">

                                    <div id="wrapD3Cube">
                                        <div id="D3Cube">
                                            <div id="side1"
                                                 style="background-image: url('/images/explore1.png')"></div>
                                            <div id="side2" style="background-image: url('/images/explore2.png')"></div>
                                            <div id="side3" style="background-image: url('/images/explore3.png')"></div>
                                            <div id="side4" style="background-image: url('/images/explore4.png')"></div>
                                            <div id="side5" style="background-image: url('/images/explore5.png')"></div>
                                            <div id="side6" style="background-image: url('/images/explore6.png')"></div>
                                        </div>
                                    </div>

                                </div>
                            </div>
                            <div class="right">
                                <div class="margin">
                                    <div class="title-step">STEP 5</div>
                                    <div class="description">
                                        Please check the final look of your NFT cube and
                                        click create if everything looks great.
                                    </div>
                                    <div class="description">
                                        Once created, subscribers will be able to purchase
                                        your NFT as a piece of art!
                                    </div>
                                    <div class="description2">
                                        *Please note that the last side of NFT cube can not be altered
                                        and it is filled with Token $hop logo automatically
                                    </div>
                                    <div class="actions">
                                        <button class="submit">Create</button>
                                    </div>
                                </div>

                            </div>

                        </div>

                    </div>
                </div>

                <div class="cord">
                    <ul class="pagination-step">
                        <li class="step_pagination" data-step="prev" style="display: none;"> <</li>
                        <li class="step_pagination select" data-step="1">1</li>
                        <li class="step_pagination" data-step="2">2</li>
                        <li class="step_pagination" data-step="3">3</li>
                        <li class="step_pagination" data-step="4">4</li>
                        <li class="step_pagination" data-step="5">5</li>
                        <li class="step_pagination second" data-step="next"> ></li>
                    </ul>
                </div>

            </form>
            <div class="_modal" id="modaladdnftclose">
                <div class="_content">
                    <div class="modal_header" style="display: flex">
                        <div class="close _hide_modal">x</div>
                    </div>
                    <div class="body">
                        <div class="modalnft-content">
                            <div class="modal-title">LEAVE PAGE?</div>
                            <div class="modal-description">
                                You didn't create your NFT yet. Are you sure you want to leave without creating NFT?
                            </div>

                            <div class="button-linebox">
                                <div class="keep">
                                    <button id="keep" class="submit _hide_modal">
                                        <div class="edit-text"> Keep creating </div>
                                    </button>
                                </div>
                                <div class="actions">
                                    <button class="submit">Leave</button>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
{{--            <div id="modaladdnftclose" class="modaladdnft _modal">--}}

{{--                <div class="modalnft-content">--}}
{{--                    <span class="close _hide_modal">x</span>--}}

{{--                    <div class="modal-title">LEAVE PAGE?</div>--}}
{{--                    <div class="modal-description">--}}
{{--                        You didn't create your NFT yet. Are you sure you want to leave without creating NFT?--}}
{{--                    </div>--}}

{{--                    <div class="button-linebox">--}}
{{--                        <div class="keep">--}}
{{--                            <button id="keep" class="submit">--}}
{{--                                <div class="edit-text"> Keep creating </div>--}}
{{--                            </button>--}}
{{--                        </div>--}}
{{--                        <div class="actions">--}}
{{--                            <button class="submit">Leave</button>--}}
{{--                        </div>--}}
{{--                    </div>--}}
{{--                </div>--}}

{{--            </div>--}}

        </div>
    </div>
@endsection
