require('./bootstrap');
require('./password');
require('./mmcash');
require('./topmenu');
require('./selected');
require('./profilemenumobile');
require('./bpprice');
require('./sortdays');
require('./sortparagraph');
require('./choose');
require('./emailpush');
require('./cube');
require('./minusplus');
require('./step');
require('./currencies');
require('./questmark');
require('./modalnft');
require('./follow');

$('.price_check').click(function () {
    $('#price_value').html($(this).data('value'));
});

$('.nft_sort').click(function () {
    $('.nft_sort.active').removeClass('active');
    $(this).addClass('active');
});

