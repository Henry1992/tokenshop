$('li.step_pagination').on('click', function () {
    $('li.step_pagination').removeClass('select');
    $(this).addClass('select');
});

//////////////////////////////////////////////////////

$(document).on('change', '.change_file', function (e) {
    $.each(this.files, (index, file) => {
        setTimeout(() => {
                if (file && file.type.indexOf('image/') === 0) {
                    var reader = new FileReader();
                    reader.onload = (e) => {
                        $('#id_test').css('background-image', 'url(' + e.target.result + ')')
                    };
                    reader.readAsDataURL(file);
                }
            }, 300
        );
    });
});

//////////////////////////////////////////////////////

// $('#step1').click(function () {
//     $('.step1').show();
//     $('#step0').hide();
//     $('#step6').show();
//     $('.step2').hide();
//     $('.step3').hide();
//     $('.step4').hide();
//     $('.step5').hide();
// });
//
// $('#step2').click(function () {
//     $('.step1').hide();
//     $('.step3').hide();
//     $('.step4').hide();
//     $('.step5').hide();
//
//     $('#step0').show();
//     $('#step6').show();
//     $('.step2').show();
// });
//
// $('#step3').click(function () {
//     $('.step1').hide();
//     $('.step2').hide();
//     $('.step4').hide();
//     $('.step5').hide();
//
//     $('#step0').show();
//     $('#step6').show();
//     $('.step3').show();
// });
//
// $('#step4').click(function () {
//     $('.step1').hide();
//     $('.step2').hide();
//     $('.step3').hide();
//     $('.step5').hide();
//
//     $('#step0').show();
//     $('#step6').show();
//     $('.step4').show();
// });
//
// $('#step5').click(function () {
//     $('.step1').hide();
//     $('.step2').hide();
//     $('.step3').hide();
//     $('.step4').hide();
//
//     $('#step0').show();
//     $('.step5').show();
//     $('#step6').hide();
// });

///
let step = 1;
$('.step_pagination').click(function () {
    stepHandler($(this).data('step'));
});
$('.arrow-box').click(function () {
    let _step = $(this).data('step');
    if (_step) {
        stepHandler(_step);
    }
});

function stepHandler(_step) {
    switch (_step) {
        case 'prev':
            step -= 1;
            break;
        case 'next':
            step += 1;
            break;
        default:
            step = parseInt(_step);
            break;
    }

    if (step > 5) {
        step = 5;
    } else if (step < 1) {
        step = 1;
    }

    if (step === 1) {
        $('.step_pagination[data-step="prev"]').hide();
    } else {
        $('.step_pagination[data-step="prev"]').show();
    }

    if (step === 5) {
        $('.step_pagination[data-step="next"]').hide();
    } else {
        $('.step_pagination[data-step="next"]').show();
    }
    $('.step_container').hide();
    $('.step_pagination.select').removeClass('select');
    $('.step_pagination[data-step="' + step + '"]').addClass('select');
    $('.step_container[data-step="' + step + '"]').show();
}





////////// procent box ///////////
$('.information-active').click(function () {
    $('.information-active.active').removeClass('active');
    $(this).addClass('active');
});
