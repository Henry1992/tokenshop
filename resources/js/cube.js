window.cubex = -22;    // initial rotation
window.cubey = -38;
window.cubez = 0;

function rotate(variableName, degrees) {
    window[variableName] = window[variableName] + degrees;
    rotCube(cubex, cubey, cubez);
}

function rotCube(degx, degy, degz) {
    segs = "rotateX(" + degx + "deg) rotateY(" + degy + "deg) rotateZ(" + degz + "deg) translateX(0) translateY(0) translateZ(0)";
    $('#D3Cube').css({"transform": segs});
}


window.turnRight = function () {
    rotate("cubey", 90);
}
window.turnLeft = function () {
    rotate("cubey", -90);
}
window.flipCube = function () {
    rotate("cubez", -180);
}

$('.change_rotate').click(function () {
    rotate("cubez", $(this).data('rotate'));
})
