let hide = true;
$('#button1').click(function (e) {
    e.preventDefault();
    menuHandler();
});
$('#button2').click(function (e) {
    e.preventDefault();
    console.log('this');
    menuHandler();
});

function menuHandler() {
    if (hide) {
        $('#button1').css('opacity', 0);
        $('#profile-menu-list').show();
    } else {
        $('#button1').css('opacity', 1);
        $('#profile-menu-list').hide();
    }
    hide = !hide;
}
